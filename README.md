##  Android Development Environment with Docker

Using Docker  solves the problem of "It works on my machine, but not on XXX machine".
The Image used provides only the bare minimum android SDK tools capable of building an android app but gives you the most flexibility in tailoring your own SDK tools for your project using volume mounting , it is based on openjdk:8 image

## Android Sdk tools contained
Path                               | Version | Description                    | Location
-------                            | ------- | -------                        | -------
platform-tools                     | 27.0.1  | Android SDK Platform-Tools     | platform-tools/
patcher;v4                         | 1       | SDK Patch Applier v4           | patcher/v4/
tools                              | 26.1.1  | Android SDK Tools              | tools/

Using the make utility programme we can ease and manage to automate builds swiftly using a Makefile
The Makefile has the following targets:
* make build - builds a new version of your Docker image and tags it
* make run  - run a new container based on created image 
* make remove - stops a running container and removes it
* make browse - to interact with the container via bash shell

### Getting Started

Get started by cloning the follwing project repositories to your local machine:  
Sample Application  
```#!/bin/bash
$ git clone git@gitlab.com:hoolu-mobile/hoolu-calc-app.git
```
Android Image  
```#!/bin/bash
$ git clone git@gitlab.com:hoolu-mobile/hoolu-android-environment.git
```

#### Install prerequisites 
To be able to follow this tutorial, you'll need
- Docker Engine (version 1.6 or higher, see https://docs.docker.com/install/linux/docker-ce/ubuntu/)
- make utility (see https://www.digitalocean.com/community/tutorials/how-to-use-makefiles-to-automate-repetitive-tasks-on-an-ubuntu-vps)

#### Step 1-Setup Environment
```#!/bin/bash
$ cd hoolu-android-environment
```
Open the build.env file and edit the values of the environment variables USERNAME,APP_DIR,SDK_DIR  
```
USERNAME=myname
APP_DIR=/path/to/myApp
SDK_DIR=/path/to/mySdk
```
Note. The SDK_DIR environent variable is important only if you are to use a predownloaded sdk as specified in step 3

#### Step 2-Build the image  
```#!/bin/bash
$ cd hoolu-android-environment
$ make build
``` 

#### Step 3-Run the container and build the app
```#!/bin/bash
$ make run
``` 

* If you are to use a predownloaded sdk, open the Makefile and mount the sdk path  to the container with **-v $(SDK_VOLUME_MOUNT)** in the  run target command   
	```Makefile
	run:   
	@echo 'Starting container' $(CONTAINER) 'based on' $(IMAGE)'... 
	@docker run --rm -it --privileged --name="$(CONTAINER)" \
                          -v $(SDK_VOLUME_MOUNT) \
                          -v $(APP_VOLUME_MOUNT) \
                          $(IMAGE):$(VERSION) bash -c './build.sh'		
		
	```

* If you are to use a gradle cache, open the Makefile and mount the gradle path to the container with **-v $(GRADLE_VOLUME_MOUNT)**   run target command in the makefile   
	```Makefile
	run:  
	@echo 'Starting container' $(CONTAINER) 'based on' $(IMAGE)'... 
	@docker run --rm -it --privileged --name="$(CONTAINER)" \
                          -v $(SDK_VOLUME_MOUNT) \
                          -v $(APP_VOLUME_MOUNT) \
                          -v $(GRADLE_VOLUME_MOUNT) \
                          $(IMAGE):$(VERSION) bash -c './build.sh'		
		
	```

#### Step 4-Stop and remove container  
```#!/bin/bash
$ make remove
``` 

#### Step 5-Access bash shell in the container
```#!/bin/bash
$ make browse
``` 

* To exit from the container without stopping it
```#!/bin/bash
root@4c2a21c559a9:/opt/workspace#  Ctrl+P && Ctrl+Q
``` 

* To exit from the container and stop it
```#!/bin/bash
root@4c2a21c559a9:/opt/workspace#  exit
```













