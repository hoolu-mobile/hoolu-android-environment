bld ?= build.env
include $(bld)
export $(shell sed 's/=.*//' $(bld))  ## sed edits the build.env and returns only the vars as shell vars wc are exported to be env vars

VERSION=latest 
PWD=$(shell pwd)
NAME=$(shell basename $(PWD))#basename gets current dir name from the absolute path
IMAGE=$(USERNAME)/$(NAME)
CONTAINER=$(NAME)_1


SDK_VOLUME_MOUNT=$(SDK_PATH):/opt/android-sdk-linux
APP_VOLUME_MOUNT=$(APP_PATH):/opt/workspace
GRADLE_VOLUME_MOUNT=$(GRADLE_CACHE_PATH):/root/.gradle/caches/


# DOCKER TASKS

# <target(function)>:<source(dependencies)> <cmd>
build: ## Build the container
	@echo 'Building image' $(IMAGE):$(VERSION)'...'
	docker build -t $(IMAGE):$(VERSION) .

	
        ## Run container 
run:	## @echo 'Starting container' $(CONTAINER) 'based on' $(IMAGE)'... vol = '$(APP_VOLUME_MOUNT)
	@docker run --rm -it --privileged --name="$(CONTAINER)"  \
                          -v $(SDK_VOLUME_MOUNT) \
                          -v $(APP_VOLUME_MOUNT) \
                          -v $(GRADLE_VOLUME_MOUNT) \
                          $(IMAGE):$(VERSION) bash -c './build.sh'		
		

stop: ## Stop and remove a running container
	@echo 'Stopping and removing' $(CONTAINER) 'container...'
	@docker stop $(CONTAINER) && docker rm $(CONTAINER)
	@echo 'Succesfully removed' $(CONTAINER) 'container'

browse: ## Enter bash
	@echo 'Bash..' $(CONTAINER) 'based on' $(IMAGE)'...
	@docker run -it --privileged --name="$(CONTAINER)" \
                          -v $(SDK_VOLUME_MOUNT) \
                          -v $(APP_VOLUME_MOUNT) \
                          $(IMAGE):$(VERSION) bash


